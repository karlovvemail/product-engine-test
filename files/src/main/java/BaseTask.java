//Copyright Geronimo DevSix


import java.io.IOException;
import java.util.Scanner;

class BaseTask {
    static {
        Thread.setDefaultUncaughtExceptionHandler(new LocalUncaughtExceptionHandler());
    }

    private static class LocalUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
        public void uncaughtException(Thread t, Throwable e) {
            System.err.println(Helper.EXCEPTION + e.getMessage() + Helper.THROWN_IN_THREAD + t + "\n");
            e.printStackTrace();
        }
    }

    protected static ClientParam getParam() throws IOException {
        ClientParam clientParam = new ClientParam();
        Scanner in = new Scanner(System.in);


        System.out.println("Input roth path");
        clientParam.setPathRoot(in.nextLine());
        //Enter data using BufferReader



        System.out.println("Input depth and input Enter");
        clientParam.setDepth(Integer.parseInt(in.nextLine()));

        // Reading data using readLine
        System.out.println("Input mask and input Enter");
        clientParam.setMask(in.nextLine());

        return clientParam;
    }
}
