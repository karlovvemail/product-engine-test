
/*
 * Copyright Alex Chulkin (c) 2016
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.Executors;

public class Task3 extends BaseTask {
	public static void main(String[] args) throws IOException {
		ServerParam serverParam = new ServerParam();
		//Enter data using BufferReader
		BufferedReader reader =
				new BufferedReader(new InputStreamReader(System.in));

		// Reading data using readLine
		System.out.println("Input root and input Enter");
		serverParam.setRoot(reader.readLine());
		System.out.println("Input port and input Enter");
		serverParam.setPort(Integer.parseInt(reader.readLine()));

		Server server = new Server(serverParam, Executors.newSingleThreadExecutor());
		server.setDispatcherExec(Executors.newCachedThreadPool()).setBrowserExec(Executors.newSingleThreadExecutor());
		server.setPrinterExec(Executors.newCachedThreadPool());
		server.execute();
		Helper.tryToSleep(Helper.MAXIMAL_WAIT);
		System.out.println(Helper.SERVER_STOPPING);
		server.stop();
	}
}
