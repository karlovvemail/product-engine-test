//Copyright Geronimo DevSix


public class ClientParam {
    private String pathRoot, mask;
    private int depth;

    public ClientParam(String root, String mask, int depth) {
        this.pathRoot = root;
        this.mask = mask;
        this.depth = depth;
    }
    public ClientParam() {
    }

    public String getPathRoot() {
        return pathRoot;
    }

    public void setPathRoot(String pathRoot) {
        this.pathRoot = pathRoot;
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }
}
